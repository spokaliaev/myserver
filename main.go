package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"net"
	"os"
)

func main() {
	err := http.ListenAndServe(":9000", handler())
	if err != nil {
		log.Fatal(err)
	}
}

func handler() http.Handler {
	r := http.NewServeMux()
	r.HandleFunc("/double", doubleHandler)
	r.HandleFunc("/version", ipGet)
	return r
}

func doubleHandler(w http.ResponseWriter, r *http.Request) {
	
	text := r.FormValue("v")
	if text == "" {
		http.Error(w, "missing value", http.StatusBadRequest)
		return
	}

	v, err := strconv.Atoi(text)
	if err != nil {
		http.Error(w, "not a number: "+text, http.StatusBadRequest)
		return
	}

	fmt.Fprintln(w, v*2)
}

func ipGet(w http.ResponseWriter, r *http.Request) {
	
	result := ""

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				result += ipnet.IP.String() + "\n"
			}
		}
	}

	fmt.Fprintln(w, result)
}